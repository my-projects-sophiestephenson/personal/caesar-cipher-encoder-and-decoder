# A simple Caesar cipher encoder and decoder

Contains two files:
- `encode.py` takes a text file and an optional integer as an argument. It prints out the file's contents shifted by the integer (mod 26), if given; otherwise, it shifts by a random key.
- `decode.py` takes a text file as an argument. It finds all 26 possible decodings of the text and prints out the most likely ones based on the number of real english words in each decoding.