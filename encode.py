##
## encode.py
## sophie stephenson
## september 2020
##
## given a text file and an integer,
## returns the text shifted to the right by that integer.
## if no integer is provided a random key is provided

## usage: python encode.py [text file] [number to shift by (optional)] 

from sys import argv
from random import randint

letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
numargs = len(argv)

try:
    f = open(argv[1], 'r')
    plaintext = f.read()
    f.close()
except:
    print("ERROR: please enter a valid filename.")
    exit()

try:
    if numargs == 3:
        shiftby = int(argv[2]) % 26

    elif numargs == 2:
        shiftby = int(randint(1, 26))

    else:
        print("ERROR: enter either 1 or 2 arguments.")
        exit()

    plaintext = plaintext.upper()
    encoded = ""

    for c in plaintext:
        if c in letters:
            index = letters.find(c)
            newindex = (index + shiftby) % 26
            encoded += letters[newindex]
        else:
            encoded += c
        
    print(encoded)

except:
    print("ERROR: the second argument must be a valid integer.")
