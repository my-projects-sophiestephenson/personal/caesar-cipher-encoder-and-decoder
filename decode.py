##
## decode.py
## sophie stephenson
## september 2020
##
## given an encoded text file, attempts to find the original message.

## usage: python decode.py [text file]

from sys import argv
import numpy as np
import enchant
from nltk.tokenize import word_tokenize

# set up
letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
error_bound = 0.8
dict = enchant.Dict("en_US")

# set up decode function
def decode(text, shift):
    decoded = ""
    for c in text:
        if c in letters:
            index = letters.find(c)
            oldindex = (index - shift) % 26
            decoded += letters[oldindex]
        else:
            decoded += c
    return decoded

# get the encoded text
try:
    f = open(argv[1], 'r')
    encoded = f.read()
    f.close()
except:
    encoded = argv[1]

numwords = len(word_tokenize(encoded))
possibilities = []

# collect possible decodings and # of real words in each
for i in np.arange(26):
    attempt = decode(encoded, i)
    num_real_words = 0
    
    tokens = word_tokenize(attempt.strip("\n"))
    for token in tokens:
        if dict.check(token):
            num_real_words += 1
    possibilities.append((attempt, i, num_real_words))

# only get the most likely answers and sort by most likely
answers = [(decoded, key) for (decoded, key, words) in possibilities
           if words >= numwords * error_bound]
answers.sort(key=lambda tup: tup[2])
answers.reverse()

print("probable decodings:")
print("-------------------")
for tup in answers:
    print("key =", tup[1], ":", tup[0].strip("\n"))
        
    
